data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int) : Comparable<MyDate> {
	override fun compareTo(comp: MyDate) = when {
        year != comp.year -> year - comp.year
        month != comp.month -> month - comp.month
        	else -> dayOfMonth - comp.dayOfMonth
    }
}

fun test(date1: MyDate, date2: MyDate) {
    println(date1 < date2)
}